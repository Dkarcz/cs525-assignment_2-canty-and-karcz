#include "dberror.h"
#include "storage_mgr.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
	
void initStorageManager(){
	//Init
}
RC createPageFile(char *fileName){
	int rc;
	FILE * fp = fopen(fileName, "w+");
	if(fp==NULL)
		return RC_FILE_NOT_FOUND;
	SM_PageHandle page = (SM_PageHandle)calloc(PAGE_SIZE, sizeof(char));
	int status = fwrite(page, sizeof(char), PAGE_SIZE, fp);

	free(page);

	if(status == 0)
		rc = RC_WRITE_FILE_FAILED;
	else{
		rc = RC_OK;
	}

	fclose(fp);
	return rc;
}
RC openPageFile(char *fileName, SM_FileHandle *fHandle){
	FILE * fp = fopen(fileName, "r+");
	if(fp == NULL)
		return RC_FILE_NOT_FOUND;

	int status = fseek(fp, 0, SEEK_END);
	if (status != 0)
		return RC_SEEK_FILE_POSITION_ERROR;

	long len = ftell(fp);
	int totalNumPages = (int)(len/PAGE_SIZE);

	status = fseek(fp, 0L, SEEK_SET);
	
	if(status != 0)
		return RC_SEEK_FILE_POSITION_ERROR;

	fHandle->fileName = fileName;
	fHandle->totalNumPages = totalNumPages;
	fHandle->curPagePos = 0;
	fHandle->mgmtInfo = fp;

	return RC_OK;
}
RC closePageFile(SM_FileHandle *fHandle){
	int status = fclose(fHandle->mgmtInfo);
	
	if(status == EOF)
		return RC_CLOSE_FILE_FAILED;

	return RC_OK;
}
RC destroyPageFile(char *fileName){
	int status = remove(fileName);
	if(status != 0)
		return RC_REMOVE_FILE_FAILED;

	return RC_OK;
}

RC readBlock(int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage){
	if(pageNum >= fHandle->totalNumPages || pageNum < 0)
		return RC_READ_NON_EXISTING_PAGE;
	
	int offset = pageNum*PAGE_SIZE*sizeof(char);
	
	int status = fseek(fHandle->mgmtInfo, offset, SEEK_SET);
	if (status != 0)
		return RC_SEEK_FILE_POSITION_ERROR;

	status = fread(memPage, sizeof(char), PAGE_SIZE, fHandle->mgmtInfo);
	if(status != PAGE_SIZE)
		return RC_READ_FILE_FAILED;

	fHandle->curPagePos = pageNum;	
	return RC_OK;
}
int getBlockPos(SM_FileHandle *fHandle){
	return fHandle->curPagePos;
}
RC readFirstBlock(SM_FileHandle *fHandle, SM_PageHandle memPage){
	return readBlock(0, fHandle, memPage);
}
RC readPreviousBlock(SM_FileHandle *fHandle, SM_PageHandle memPage){
	int pageNum = fHandle->curPagePos - 1;
	return readBlock(pageNum, fHandle, memPage);
}
RC readCurrentBlock(SM_FileHandle *fHandle, SM_PageHandle memPage){
	int pageNum = fHandle->curPagePos;
	return readBlock(pageNum, fHandle, memPage);
}
RC readNextBlock(SM_FileHandle *fHandle, SM_PageHandle memPage){
	int pageNum = fHandle->curPagePos + 1;      
	return readBlock(pageNum, fHandle, memPage);
}
RC readLastBlock(SM_FileHandle *fHandle, SM_PageHandle memPage){
	int pageNum = fHandle->totalNumPages - 1;
	return readBlock(pageNum, fHandle, memPage);
}

RC writeBlock(int pageNum, SM_FileHandle *fHandle, SM_PageHandle memPage){
	if(pageNum >= fHandle->totalNumPages || pageNum < 0)
		return RC_READ_NON_EXISTING_PAGE;
	
	int offset = pageNum*PAGE_SIZE*sizeof(char);

	int status = fseek(fHandle->mgmtInfo, offset, SEEK_SET);
	if(status != 0)
		return RC_SEEK_FILE_POSITION_ERROR;
	
	status = fwrite(memPage, sizeof(char), PAGE_SIZE, fHandle->mgmtInfo);
	if(status != PAGE_SIZE)
		return RC_WRITE_FILE_FAILED;
	return RC_OK;
}
RC writeCurrentBlock (SM_FileHandle *fHandle, SM_PageHandle memPage){
	int pageNum = fHandle->curPagePos;
        return writeBlock(pageNum, fHandle, memPage);
}
RC appendEmptyBlock (SM_FileHandle *fHandle){
	int rc = -99;
        int pageNum = fHandle->totalNumPages;
        SM_PageHandle page = (SM_PageHandle)calloc(PAGE_SIZE, sizeof(char));

	int status = fseek(fHandle->mgmtInfo, 0L, SEEK_END);
	if(status!=0)
		return RC_SEEK_FILE_POSITION_ERROR;
	
	status = fwrite(page, sizeof(char), PAGE_SIZE, fHandle->mgmtInfo);
	if(status!=PAGE_SIZE)
		rc= RC_WRITE_FILE_FAILED;
	fHandle->totalNumPages = pageNum++;
	fHandle->curPagePos = pageNum;
        rc= RC_OK;

	free(page);
	return rc;

}
RC ensureCapacity (int numberOfPages, SM_FileHandle *fHandle, int * uninsuredCapacity){
	if(numberOfPages <= fHandle->totalNumPages)
		return RC_OK;

	int totalNumPages = fHandle->totalNumPages;
	int pagesToEnsure = numberOfPages-totalNumPages;
	int i;
	for(i = 0; i < pagesToEnsure; i++){
		RC rc = appendEmptyBlock(fHandle);
		if(rc!=RC_OK)
			*uninsuredCapacity = pagesToEnsure-i;
			return rc;
	}
	*uninsuredCapacity = pagesToEnsure-i;
	return RC_OK;

}


